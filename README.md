# Specfem2D

Specfem2D快速上手指南

$ 之后就是需要在终端中输入的命令

# 安装

### 1.下载：

1.1 利用git下载specfem2D的源码 

`$ git clone --recursive --branch devel https://github.com/geodynamics/specfem2d.git`

1.2 考虑到git在国内下载速度过慢，可选择下载ZIP文件到本地

注意：选择branch为devel

#### 2.安装：

2.1 解压

`$ unzip specfem2d-devel.zip`

2.2 切换目录

`$ cd specfem2d-devel`

2.3 配置环境(FC与CC分别表示使用的fortran与C语言编译器，根据需要可以选择gfortran/gcc与ifort/icc的组合)

`$ ./configure FC=gfortran CC=gcc`

2.4 编译

`$ make`

所有生成的可执行程序都在./bin/中

#### 3.测试：

`$ ./bin/xmeshfem2D`

`$ ./bin/xspecfem2D`

# 使用

以./EXAMPLES/initial_plane_wave_with_free_surface为例，

切换至该目录

`$ cd ./EXAMPLES/initial_plane_wave_with_free_surface`

使用`vi`修改Par_file，NPROC改为1，initialfield改为false，`esc`+`:`+`wq`保存并退出。

`./run_this_example.sh`，进行计算过程，结束之后会生成DATA与OUTPUT_FILES两个文件夹

OUTPUT_FILES中的*.semd为生成的地震记录，可以通过get-sac-from-semd.py将其转化为sac可读的SAC文件，*.jpg为位移场的snapshot，100个采样点会生成一个snapshot。

get-sac-from-semd.py使用说明：

`$ python get-sac-from-semd.py semd`


# 具体参数设置

#### 1.震源有关的设置项：
1.1 SOURCE文件中的设置项：

| 设置项 | 赋值 | 解释 |
| --- | :--: | :---: |
| source_surf | .false. | 决定了震源是否在模型表面，false表示不在模型表面 |
| xs | +150000 | 给定震源在x方向的位置，单位是米 |
| zs | 250000 | 给定震源在z方向的位置，单位是米 |
| source_type | 1/2 | 震源表示的类型，1为震源时间函数，2为Moment tensor |
| time_function_type | 1/2/3/4/5/8/9 | 震源时间函数的类型 |
| f0 | 0.4 | 设置震源的主频 |

1.2 Par_file中的设置项：

| 设置项 | 赋值 | 解释 |
| --- | :--: | :---: |
| NSOURCES | 1 | 震源的个数 |
|initialfield | .true. | 是否用已存在的波场当作震源，true表示是 |

#### 2.台站有关的设置项：

Par_file中的设置项：

| 设置项 | 赋值 | 解释 |
| --- | :--: | :---: |
| use_existing_STATIONS | .false. | false表示使用Par_file中的信息给出台站位置，true表示使用DATA中的STATIONS文件 |
| nreceiversets | 1 | 测线的条数 |
| nrec | 30 | 台站的个数 |
| xdeb/zdeb | 500000/250000 | 第一个台站的位置 |
| xfin/zfin | 100000/250000 | 最后一个台站的位置 |

这样的设置表示在第一个和最后一个台站中间均匀布台。

#### 3.模型有关的设置项：

Par_file中的设置项：

| 设置项 | 赋值 | 解释 |
| --- | :--: | :---: |
| MODEL | default | 表示根据Par_file中模型信息建立密度-速度模型 |
| nbmodels | n | 给出介质类型的个数，如果是n层模型，则设定为n |
| PML_BOUNDARY_CONDITIONS | .false. | 表示模型的边界是否为PML吸收边界，false为不是，与下方的STAECY属于二选一关系 |
| xmin/xmax/nx | 0.d0/600000.d0/120 | 给出模型在x方向的网格化，这里给出的参数表示x轴方向由0-600000米内均匀分为120个网格，每个网格x轴长度为5000米 | 
| nbregions | 1/ 1 120 1 50 1 | 表示x方向有120个网格单位，z方向有50个网格单位，这样整个区域被划分为120*50个网格，整个区域的速度-密度关系由上方的`nbmodels`给出 |
#### 4.常规设置项：

Par_file中的设置项：

| 设置项 | 赋值 | 解释 |
| --- | :--: | :---: |
| NPROC | 1 | 表示程序运行时单核计算，如果多核并行运行可以设置为大于1的数字，但需要额外安装MPI <https://www.open-mpi.org/> |  
| DT | 0.01 | 类似于地震数据中采样率的概念，采样频率，默认为0.01s |
| NSTEP | 6000 | 表示总共需要计算多少个采样点，默认是6000个采样点，也就是60s的记录 |
